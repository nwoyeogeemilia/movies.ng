<?php

namespace App\Utilities;

use Illuminate\Database\Eloquent\Model;

trait ControllerTraits
{
    /**
     * @var
     */
    protected $path;

    /**
     * @var
     */
    protected $entity;
}
