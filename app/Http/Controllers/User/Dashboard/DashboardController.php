<?php

namespace App\Http\Controllers\User\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * DashboardController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:web');
        $this->path = 'dashboard.';
    }

    /**
     * @return string
     */
    public function index()
    {
    return view($this->path.'.index');
    }
}
