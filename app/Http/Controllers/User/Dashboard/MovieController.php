<?php

namespace App\Http\Controllers\User\Dashboard;

use App\Http\Controllers\Controller;
use App\Movie;
use Illuminate\Http\Request;
use Illuminate\View\View;
use phpDocumentor\Reflection\Types\Boolean;

class MovieController extends Controller
{
    /**
     * MovieController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:web');
        $this->path = 'movies.';
        $this->entity = new Movie();
    }

    public function index()
    {
        return view($this->path.'.index')->withMovies($this->entity->all());
    }

    public function save(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'banner' => 'required|image|mimes:png,jpg,jpeg',
            'description' => 'required|string'
        ]);
        $code = rand(1234567, 9999999);

        $file = $request->file('banner');
        $file_name = 'uploads/movies/'.$code.str_slug($request->name).'.'.$file->getClientOriginalExtension();
        $file->move(public_path('uploads/movies/'), $code.str_slug($request->name).'.'.$file->getClientOriginalExtension());



        try{
            $exists = $this->entity->whereName($request->name)->first();

            if($exists){
                session()->flash('warning', 'The selected Movie Name is already in use');
                return redirect()->back()->withInput($request->all());
            }

            $this->entity->create([
                'name' => $request->name,
                'photo' => $file_name,
                'description' => $request->description,
                'slug' => str_slug($request->name).'-'.$code
            ]);
        }
        catch(\Exception $exception){
            session()->flash('danger', $exception->getMessage());
            return redirect()->back()->withInput($request->all());
        }

        session()->flash('success', 'Movie has been created successfully');
        return redirect()->back();

    }

    public function viewMovie($slug)
    {
        return view($this->path.'view')->withMovie($this->entity->whereSlug($slug)->first());
    }
}
