@extends('layouts.app')

@section('styles')
    <link href="{{ asset('assets/bootstrap/css/dashboard.css') }}" type="text/css" rel="stylesheet">
    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
@endsection

@section('content')




    <div class="px-4 py-5" id="custom-cards">
        @include('layouts._partials._alert')
        @include('layouts._partials._errors')
        <h5 class="pb-2 border-bottom">Add Movie</h5>
        <form class="row align-items-center" method="post" action="{{ route('user.movies.save') }}" enctype="multipart/form-data">
            @csrf
        <div class="col-4">
            <div class="input-group">
                <span class="input-group-text" id="basic-addon1">Movie Name</span>
                <input type="text" name="name" class="form-control" placeholder="Movie Name" aria-label="Username" aria-describedby="basic-addon1" value="{{ old('name') }}">
            </div>
        </div>
        <div class="col-8">
            <div class="input-group mb-3">
                <input type="file" name="banner" class="form-control" id="inputGroupFile01" accept="images/*">
            </div>
        </div>
            <div class="col-12">
                <div class="input-group">
                    <span class="input-group-text">Movie Description</span>
                    <textarea class="form-control" name="description" aria-label="With textarea">{{ old('description') }}</textarea>
                </div>
            </div>
            <div class="mt-3">
                <button class="btn btn-primary btn-sm float-end"><i class="fa fa-plus-square-o"></i>Create Movie</button>
            </div>
        </form>


        <h2 class="pb-2 border-bottom">Movies And Ratings</h2>
        <div class="row row-cols-1 row-cols-lg-3 align-items-stretch g-4 py-5">
            @foreach($movies as $movie)
            <div class="col">
                <div class="card card-cover h-100 overflow-hidden text-white bg-dark rounded-5 shadow-lg" style="background-image: url('{{asset($movie->photo)}}');background-repeat: no-repeat;background-size: cover;">
                    <div class="d-flex flex-column h-100 p-5 pb-3 text-white text-shadow-1">
                        <h5 class="pt-5 mt-5 mb-4 display-6 lh-1 font-bold">{{$movie->name}}</h5>
                        <ul class="d-flex list-unstyled mt-auto">
                            <li class="me-auto">
                                <a href="{{route('user.movies.view', $movie->slug )}}"><img src="https://github.com/twbs.png" alt="Bootstrap" width="32" height="32" class="rounded-circle border border-white"></a>
                            </li>
                            <li class="d-flex align-items-center me-3">

                                <small>Earth</small>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
    </div>
    <div class="b-example-divider"></div>

@endsection
