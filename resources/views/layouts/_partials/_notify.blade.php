@if(session()->has('message'))
    <script>
        let content = {};

        content.message = '{{ session()->get('message')['content'] }}';
        content.title = '{{ session()->get('message')['title'] ?? 'Request complete!' }}';
        content.icon = '{{ session()->get('message')['icon'] ?? 'fas fa-bell' }}';

        $.notify(content,{
            type: '{{ session()->get('message')['state'] ?? 'default' }}',
            placement: {
                from: '{{ session()->get('message')['from'] ?? 'bottom' }}',
                align: '{{ session()->get('message')['align'] ?? 'right' }}'
            },
            time: 1500,
            delay: '{{ session()->get('message')['delay'] ?? 5000 }}',
        });
    </script>
@endif
