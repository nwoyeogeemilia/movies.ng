@if($errors->any())
    <ul class="list-group list-group-flush mb-3">
        @foreach ($errors->all() as $error)
            <li class="list-group-item list-group-item-danger bg-red font-12 text-left">
                <i class="fa fa-info-circle mt-1 mr-2"></i> {{ $error }}
            </li>
        @endforeach
    </ul>
@endif
