@if(session()->has('success'))
    <script>
        Swal.fire({
            title: 'Good job!',
            text: "{{ session()->get('success') }}",
            type: 'success',
            confirmButtonClass: 'btn btn-success',
        })
    </script>
@endif

@if(session()->has('info'))
    <script>
        Swal.fire(
            'Heads Up',
            "{{ session()->get('info') }}",
            'question'
        );
    </script>
@endif

@if(session()->has('danger'))
    <script>
        Swal.fire({
            title: "Heads Up!",
            text: "{{ session()->get('danger') }}",
            type: "error",
            confirmButtonClass: 'btn btn-success',
        });
    </script>
@endif

@if(session()->has('warning'))
    <script>
        Swal.fire({
            title: "Oops!",
            text: "{{ session()->get('warning') }}",
            type: "warning",
            confirmButtonClass: 'btn btn-success',
        });
    </script>
@endif
