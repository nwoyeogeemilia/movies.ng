@if($entity->count() == 0)
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-10 offset-md-1 text-center">
                    <i class="fas fa-info-circle font-50"></i>
                    <h2>We couldn't find any data</h2>
                    <p class="lead">
                        Sorry we can't find any data, to get rid of this message, make at least 1 entry.
                    </p>
                    <a href="{{ url()->current() }}" class="btn btn-primary mt-4">Refresh Page</a>
                </div>
            </div>
        </div>
    </div>
@endif
