<?php

use Illuminate\Support\Facades\Route;




/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::namespace('User')->group(function () {

    Route::namespace('Auth')->group(function () {

        Route::get('/register', 'RegisterController@showRegistrationForm')->name('register');

        Route::post('/register', 'RegisterController@register')->name('user.register');
        Route::get('/login', 'LoginController@showLoginForm')->name('login');
        Route::post('/login', 'LoginController@login')->name('user.login');
        Route::post('/logout', 'LoginController@logout')->name('logout');
    });

    //Dashboard Routes
    Route::namespace('Dashboard')->group(function (){
        Route::group(['prefix' => 'dashboard', 'middleware' => 'auth:web'], function(){
            Route::get('/', 'DashboardController@index')->name('user.dashboard');

            //Movie Routes
            Route::group(['prefix' => 'movies'], function(){
                Route::get('/', 'MovieController@index')->name('user.movies');
                Route::post('/save', 'MovieController@save')->name('user.movies.save');
                Route::get('/{slug}', 'MovieController@viewMovie')->name('user.movies.view');

            });
        });
    });





});




